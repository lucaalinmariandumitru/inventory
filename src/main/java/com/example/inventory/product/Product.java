package com.example.inventory.product;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
@Table
public class Product {
    @Column
    private Double price;
    @Column
    private Integer weight;
    @Column
    private String name;

 @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
}
